package com.jauto.view.ui;

import com.jauto.view.screens.MenuScreenController;
import com.jauto.view.utilities.ViewUtilities;
import com.jauto.view.utilities.enums.NotificationType;

import eu.hansolo.enzo.notification.NotificationEvent;
import javafx.event.EventHandler;

/**
 * Inteface only for {@link MenuScreenController}.
 * <p>
 * The controllers of the {@link ViewUtilities} must implements this interface.
 */
public interface UIMenu {

    /**
     * Show a popup message with title and gravity.
     *
     * @param title
     *            the {@link String} title
     * @param message
     *            the {@link String} message
     * @param secondsDuration
     *            the {@link Duration} duration.
     * @param notiType
     *            the {@link NotificationType} duration
     * @param ev
     *            the {@link NotificationEvent} event
     */
    void showNotificationPopup(String title, String message, NotificationType.Duration secondsDuration,
            NotificationType notiType, EventHandler<NotificationEvent> ev);

    /**
     * Update view.
     *
     * @param inputLine
     *            the input
     * @return
     */
    void updateViewReceived(String inputLine);
}