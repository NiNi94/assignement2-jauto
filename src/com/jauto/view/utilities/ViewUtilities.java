package com.jauto.view.utilities;

import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;

import com.jauto.view.utilities.enums.IconDim;
import com.jauto.view.utilities.enums.NotificationType;

import eu.hansolo.enzo.notification.Notification;
import eu.hansolo.enzo.notification.NotificationEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

/**
 * This class has all the utilities for the view.
 * <p>
 * For example you can set icons in a very simple way and many more options.
 *
 */
public final class ViewUtilities {

    private ViewUtilities() {
    }

    /**
     * A simple method for charging {@link Ikon} in {@link Node}.
     *
     * @param icon
     *            the {@link Ikon}
     * @param fontSize
     *            the font size
     * @return {@link IconDim} the {@link IconDim}
     */
    public static FontIcon iconSetter(final Ikon icon, final IconDim fontSize) {
        final FontIcon tempIcon = new FontIcon(icon);
        tempIcon.setIconSize(fontSize.getDim());
        return tempIcon;
    }

    /**
     * Show a {@link Notification} popup into the main windows of the operating system.
     *
     * @param title
     *            the String title of the {@link Notification}
     * @param message
     *            the String text of the {@link Notification}
     * @param secondsDuration
     *            the number of {@link com.jlearn.view.utilities.enums.NotificationType.Duration} of the
     *            {@link Notification}
     * @param notiType
     *            the {@link NotificationType} of the {@link Notification}
     * @param ev
     *            the {@link EventHandler} ev, lalmba
     */
    public static void showNotificationPopup(final String title, final String message,
            final NotificationType.Duration secondsDuration, final NotificationType notiType,
            final EventHandler<NotificationEvent> ev) { // _____________________________PATTERN STRATEGY

        final Notification.Notifier no = Notification.Notifier.INSTANCE;
        final Notification notification = new Notification(title, message);

        no.setPopupLifetime(Duration.seconds(secondsDuration.getValue()));
        switch (notiType) {
        case ERROR:
            no.notifyError(title, message);
            break;
        case WARNING:
            no.notifyWarning(title, message);
            break;
        case SUCCESS:
            no.notifySuccess(title, message);
            break;
        case INFO:
            no.notifyInfo(title, message);
            break;
        default:
            no.notify(notification);
            break;
        }
        no.setOnNotificationPressed(ev);
    }
}
