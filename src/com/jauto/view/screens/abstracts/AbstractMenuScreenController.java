package com.jauto.view.screens.abstracts;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.kordamp.ikonli.material.Material;

import com.jauto.view.FXEnvironment;
import com.jauto.view.factories.GeneralFactory;
import com.jauto.view.screens.FXMLScreens;
import com.jauto.view.utilities.ViewUtilities;
import com.jauto.view.utilities.enums.IconDim;
import com.jauto.view.utilities.enums.NotificationType;
import com.jauto.view.voice_recognition.VoiceRecognition;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXSlider.IndicatorPosition;
import com.jfoenix.controls.JFXToggleButton;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;

/**
 * Menu controller for the Menu.
 * <p>
 * This class represent a symple controller for a {@link FXMLScreens}. The fxml is atached to the controller by the
 * {@link FXEnvironment} and chached by the {@link FXMLLoader}.
 * <p>
 * This controller is connected to a {@link ControllerExercise} to implement a MVC design.
 *
 */
public abstract class AbstractMenuScreenController {

    // private static final double MENU_ITEM_SIZE = 50;
    private static final Rectangle2D PRIMARY_SCREEN_BOUNDS = Screen.getPrimary().getVisualBounds();
    private static final int APP_WIDTH = 500;
    private static final int APP_HEIGHT = 600;
    private static final float SCALING_CONSTANT = 1.2f;
    private static final int DELTA = 50;
    private static final int FULL_SCREEN_TRIGGER = 50;

    // ################################################ FXML VAR ###############################################
    @FXML
    private BorderPane nodeBorderMenu; // NOPMD
    @FXML
    private AnchorPane anchorPane; //NOPMD
    @FXML
    private JFXToggleButton buttonCredits;
    @FXML
    private JFXButton menuButton;

    private final JFXPopup menuPopup = new JFXPopup();
    private final JFXListView<Label> menuListView = new JFXListView<>();

    // ################################################ DRAWER VAR ###############################################
    @FXML
    private JFXDrawer drawerLevels; // DRAWER FOR LEVELS
    // ################################################ POPUP VAR ###############################################
    private JFXPopup sliderPopup;
    private JFXPopup sliderVolumePopup;
    private JFXSlider horRightSlider;

    // ################################################ LOCAL VAR ###############################################
    private final FXEnvironment environment;

    /**
     * The constructor of the controller.
     *
     * @param environment
     *            the {@link FXEnvironment}
     */
    public AbstractMenuScreenController(final FXEnvironment environment) {
        this.environment = environment;

    }

    /**
     * Init method to be called after the constructor. This is a symple method of the {@link FXML}.
     *
     * @throws IOException
     *             Expception
     */
    @FXML
    protected void initialize() throws IOException { // NOPMD
        this.initPopup();
        this.initSliderPopup();
        this.initListDrawerView();
        this.initButtons();
    }

    // ################################################ INIT ###############################################

    private void initButtons() {
        this.menuButton.setTooltip(new Tooltip("Open the menu"));
        this.menuButton.setGraphic(ViewUtilities.iconSetter(Material.MORE_VERT, IconDim.MEDIUM));
    }

    private void initListDrawerView() {
        //JFXDepthManager.setDepth(this.drawerLevels, 10);
        this.anchorPane.setOnMouseClicked(t -> {
            this.drawerLevels.close();
            if (this.buttonCredits.isSelected()) {
                this.buttonCredits.fire();
            }
        });
    }

    private void initPopup() {
        GeneralFactory.buildPopup(this.menuButton, this.menuPopup, this.menuListView, FXMLScreens.MENU);
        this.menuButton.setOnMouseClicked((e) -> {
            this.menuPopup.show(this.menuButton);
        });

        // CHECKSTYLE:OFF DAI E' SONO COSTRETTO
        this.menuListView.setOnMouseClicked(e -> {
            this.menuPopup.hide();
            if (this.menuListView.getSelectionModel().getSelectedIndex() == 0) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().browse(new URI("https://bitbucket.org/NiNi94/oop16-jlearn"));
                    } catch (IOException | URISyntaxException e1) {
                        System.out.println(e1);
                    }
                } else {
                    this.environment.showNotificationPopup("Internet Error", "Browser not found",
                            NotificationType.Duration.MEDIUM, NotificationType.WARNING, null);
                }
            } else if (this.menuListView.getSelectionModel().getSelectedIndex() == 1) {
                this.showResizeScreenPopup(this.nodeBorderMenu);
            } else if (this.menuListView.getSelectionModel().getSelectedIndex() == 2) {
                this.fullScreenOn();
            } else if (this.menuListView.getSelectionModel().getSelectedIndex() == 3) {
                this.fullScreenOff();
            } else if (this.menuListView.getSelectionModel().getSelectedIndex() == 4) {
                VoiceRecognition.getInstance().toggleRecognize();
                this.environment.showNotificationPopup("Recognizer On",
                        "Words: menu, theory, exercise, \n voice off, menu, exit application",
                        com.jauto.view.utilities.enums.NotificationType.Duration.VERY_LONG,
                        NotificationType.INFO, null);
            } else if (this.menuListView.getSelectionModel().getSelectedIndex() == 5) {
                System.exit(0);
                // CHECKSTYLE:ON
            }
        });
    }

    private void initSliderPopup() {
        this.horRightSlider = new JFXSlider();
        this.horRightSlider.setIndicatorPosition(IndicatorPosition.RIGHT);
        this.horRightSlider.setMin(APP_WIDTH);
        final HBox box = new HBox(this.horRightSlider);
        box.setId("popup-box");
        this.sliderPopup = new JFXPopup(box);
        this.horRightSlider.setMax(PRIMARY_SCREEN_BOUNDS.getWidth());
        this.horRightSlider.setId("jfx-slider-style");
    }

    // INIT THE SCALE ENGINE IT'S A LAMBDA THREAD THAT BIND AND MANAGE STAGE DIMENSIONS
    private void scaleEngine() {
        Platform.runLater(() -> {
            this.horRightSlider.setValue((int) this.environment.getMainStage().getWidth() + DELTA);

            this.horRightSlider.valueProperty().addListener((observable, newValue, oldValue) -> {
                if ((newValue.intValue() < (this.horRightSlider.getMax() - FULL_SCREEN_TRIGGER))) {
                    if ((((int) this.environment.getMainStage()
                            .getHeight()) >= ((int) (PRIMARY_SCREEN_BOUNDS.getHeight() - FULL_SCREEN_TRIGGER)))
                            && (((int) this.environment.getMainStage().getWidth()
                                    * SCALING_CONSTANT) > (int) PRIMARY_SCREEN_BOUNDS.getHeight())) {
                        this.environment.getMainStage().setWidth(newValue.intValue());
                    } else {
                        this.environment.getMainStage().setWidth(newValue.intValue());
                        this.environment.getMainStage().setHeight((int) (newValue.intValue() * SCALING_CONSTANT));
                    }
                } else {
                    this.fullScreenOn();
                }
                if (newValue.intValue() < (APP_WIDTH + DELTA)) {
                    this.fullScreenOff();
                }
            });
        });
    }

    // #################################### PUBLIC METHODS ####################################

    /**
     * Slider screen popup.
     *
     * @param container
     *            the {@link Pane} popup container
     */
    public void showResizeScreenPopup(final Pane container) {
        this.sliderPopup.show(container);
        this.scaleEngine(); // Sarts the scale engine (must be done runtime beacuse access to main Stage)
    }

    /**
     * Slider volume popup.
     *
     * @param container
     *            the {@link Pane} popup container
     */
    public void showVolumePopup(final Pane container) {
        this.sliderVolumePopup.show(container);
    }

    // ################################################ HANDLERS ###############################################

    /**
     * Toggle the full screen off.
     */
    public void fullScreenOff() {
        this.environment.fadeTransition();
        Platform.runLater(() -> {
            this.environment.getMainStage().setHeight(APP_HEIGHT);
            this.environment.getMainStage().setWidth(APP_WIDTH);
            this.environment.getMainStage().setX((PRIMARY_SCREEN_BOUNDS.getMaxX() / 2) - (APP_WIDTH / 2));
            this.environment.getMainStage().setY((PRIMARY_SCREEN_BOUNDS.getMaxY() / 2) - (APP_HEIGHT / 2));
        });
    }

    /**
     * Toggle the full screen on.
     */
    public void fullScreenOn() {
        this.environment.fadeTransition();
        Platform.runLater(() -> {
            this.environment.getMainStage().setHeight(PRIMARY_SCREEN_BOUNDS.getHeight());
            this.environment.getMainStage().setWidth(PRIMARY_SCREEN_BOUNDS.getWidth());
            this.environment.getMainStage().setX(0);
            this.environment.getMainStage().setY(0);
        });
    }

    @FXML
    private void showDrawerModules() { // NOPMD
        if (this.buttonCredits.isSelected()) {
            this.drawerLevels.open();
        } else {
            this.drawerLevels.close();
        }
    }

    // ################################################ ABSTRACT ###############################################
    /**
     * Init {@link JFXListView} with avaiable ports.
     */
    protected abstract void initListView();
}
