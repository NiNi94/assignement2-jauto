package com.jauto.view.screens;

import java.io.IOException;

import org.kordamp.ikonli.material.Material;

import com.jauto.controller.Controller;
import com.jauto.view.FXEnvironment;
import com.jauto.view.screens.abstracts.AbstractMenuScreenController;
import com.jauto.view.ui.UIMenu;
import com.jauto.view.utilities.ViewUtilities;
import com.jauto.view.utilities.enums.IconDim;
import com.jauto.view.utilities.enums.NotificationType;
import com.jauto.view.utilities.enums.NotificationType.Duration;
import com.jauto.view.voice_recognition.EObserver;
import com.jauto.view.voice_recognition.ESource;
import com.jauto.view.voice_recognition.VoiceCommands;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;

import eu.hansolo.enzo.notification.NotificationEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Menu controller for the Menu.
 * <p>
 * This class represent a symple controller for a {@link FXMLScreens}. The fxml is atached to the controller by the
 * {@link FXEnvironment} and chached by the {@link FXMLLoader}.
 * <p>
 * This controller is connected to a {@link ControllerExercise} to implement a MVC design.
 *
 */
public final class MenuScreenController extends AbstractMenuScreenController implements UIMenu, EObserver<String> {

    // ################################################ FXML VAR ###############################################
    private final JFXListView<Label> drawerListView = new JFXListView<>();
    @FXML
    private JFXDrawer drawerLevels; // DRAWER FOR LEVELS
    @FXML
    private JFXTextField inputTextField; //NOPMD
    @FXML
    private JFXTextField outputTextField; //NOPMD
    @FXML
    private JFXButton openButton, stopButton;

    private JFXButton refresh; //NOPMD
    private VBox drawerPane; //NOPMD
    private Label title; //NOPMD
    @FXML
    private JFXToggleButton buttonCredits;

    // ################################################ LOCAL VAR ###############################################
    private final FXEnvironment environment;
    private final Controller controller;

    /**
     * The constructor of the controller.
     *
     * @param environment
     *            the {@link FXEnvironment}
     * @param controller
     *            the {@link Controller}
     * @throws IOException
     *             the exception cause by the loading of the {@link FXMLLoader}
     */
    public MenuScreenController(final FXEnvironment environment, final Controller controller) {
        super(FXEnvironment.getInstance());
        this.environment = environment;
        this.controller = controller;
        this.environment.loadScreen(FXMLScreens.MENU, this);
        this.controller.getClass();

    }

    @Override
    @FXML
    protected void initialize() throws IOException { // NOPMD
        super.initialize();
        this.initListView();
        this.initButton();
    }

    /**
     * Show.
     */
    public void show() {
        this.environment.displayScreen(FXMLScreens.MENU);
    }

    private void initButton() {
        this.openButton.setGraphic(ViewUtilities.iconSetter(Material.LOCK_OPEN, IconDim.BIG));
        this.stopButton.setGraphic(ViewUtilities.iconSetter(Material.STOP, IconDim.BIGGER));
        this.openButton.setTooltip(new Tooltip("Send Open"));
        this.stopButton.setTooltip(new Tooltip("Send Stop"));

        this.openButton.setOnMouseClicked(e -> {
            this.controller.sendData("O" + "\n");
            this.environment.showNotificationPopup("COMANDI VOCALI!",
                    "1) Open menu select: Voice Commands\n"
                            + "2) Say: OPEN v CLOSE v EXITAPPLICATION",
                    Duration.MEDIUM, NotificationType.INFO, null);
        });
        this.stopButton.setOnMouseClicked(e -> {
            this.controller.sendData("S" + "\n");
            this.environment.showNotificationPopup("COMANDI VOCALI!",
                    "1) Open menu select: Voice Commands\n"
                            + "2) Say: OPEN v CLOSE v EXITAPPLICATION",
                    Duration.MEDIUM, NotificationType.INFO, null);
        });
    }

    @Override
    public void showNotificationPopup(final String title, final String message, final Duration secondsDuration,
            final NotificationType notiType,
            final EventHandler<NotificationEvent> ev) {
    }

    /**
     * Voice Recognition Observer Pattern.
     *
     * @param src
     *            the {@link ESource} source
     * @param arg
     *            the {@link String} argument
     */
    @Override
    public void update(final ESource<? extends String> src, final String arg) {
        if (arg.equals(VoiceCommands.EXIT.getVoiceCommand())) {
            Runtime.getRuntime().exit(0);
        } else if (arg.equals(VoiceCommands.OPEN.getVoiceCommand())) {
            this.controller.sendData("O" + "\n");
        } else if (arg.equals(VoiceCommands.STOP.getVoiceCommand())) {
            this.controller.sendData("S" + "\n");
        }
    }

    @Override
    protected void initListView() {
        this.drawerListView.setOnMouseClicked(this::handleListView);
        this.drawerListView.setId("listViewPort");
        this.drawerPane = new VBox();
        this.title = new Label("Avaiable Ports");
        this.refresh = new JFXButton();
        this.refresh.setId("refreshButton");
        this.title.setId("titlePort");
        this.drawerPane.getChildren().addAll(this.title, this.drawerListView, this.refresh);
        this.drawerLevels.setSidePane(this.drawerPane);
        VBox.setVgrow(this.drawerListView, Priority.ALWAYS);
        VBox.setVgrow(this.refresh, Priority.ALWAYS);
        this.controller.getPortNames().stream()
                .forEach(t -> this.drawerListView.getItems().add(new Label(t.getName())));
        this.refresh.setGraphic(ViewUtilities.iconSetter(Material.REFRESH, IconDim.MEDIUM));

        this.refresh.setOnMouseClicked(this::handleRefresh);
        this.inputTextField.setOnAction(this::handleTextField);

    }

    private void handleListView(final MouseEvent e) { //NOPMD
        final String portName = this.drawerListView.getItems()
                .get(this.drawerListView.getSelectionModel().getSelectedIndex()).getText();
        this.buttonCredits.setText(" " + portName);
        this.controller.openPort(portName);

    }

    private void handleRefresh(final MouseEvent e) { //NOPMD
        this.drawerListView.getItems().clear();
        this.controller.getPortNames().stream()
                .forEach(t -> this.drawerListView.getItems().add(new Label(t.getName())));
        this.environment.showNotificationPopup("Refresh", "Refreshed", Duration.MEDIUM, NotificationType.INFO, null);
    }

    private void handleTextField(final ActionEvent e) { //NOPMD
        this.controller.sendData(this.inputTextField.getText() + "\n");
        this.inputTextField.clear();
    }

    @Override
    public void updateViewReceived(final String inputLine) {
        this.outputTextField.setText(inputLine);
    }

}
