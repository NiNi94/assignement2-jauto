package com.jauto.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import com.jauto.controller.Controller;
import com.jauto.view.FXEnvironment;
import com.jauto.view.utilities.enums.NotificationType;
import com.jauto.view.utilities.enums.NotificationType.Duration;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import javafx.application.Platform;

/**
 * Simple Serial Monitor, adaptation from:
 *
 * http://playground.arduino.cc/Interfacing/Java.
 *
 */
public class SerialMonitor implements SerialPortEventListener {
    private final Set<Controller> uis;
    private SerialPort serialPort;

    /**
     * A BufferedReader which will be fed by a InputStreamReader converting the bytes into characters making the
     * displayed results codepage independent
     */
    private BufferedReader input;
    /** The output stream to the port */
    private OutputStream output; //NOPMD
    /** Milliseconds to block while waiting for port open */
    private static final int TIME_OUT = 2000;
    private static final int RATE = 9600;

    private String portName;
    private int dataRate;

    /**
     * Constructor.
     *
     * @param portName
     *            the {@link String} port name
     */
    public SerialMonitor(final String portName) {
        this.dataRate = RATE;
        this.portName = portName;
        this.uis = new HashSet<>();
    }

    /**
     * Constructor.
     *
     * @param portName
     *            the {@link String} port name
     * @param dataRate
     *            the data rate
     */
    public SerialMonitor(final String portName, final int dataRate) {
        this.dataRate = dataRate;
        this.portName = portName;
        this.uis = new HashSet<>();
    }

    /**
     * Add observer.
     *
     * @param observer
     *            the observer
     */
    public void setObserver(final Controller observer) {
        this.uis.add(observer);
    }

    /**
     * Start method.
     *
     */
    public void start() {
        CommPortIdentifier portId = null;
        System.out.println("started\nPort: " + this.portName + "\nBauldRate: " + this.dataRate);

        try {
            portId = CommPortIdentifier.getPortIdentifier(this.portName);
            System.out.println("Port ID: " + portId);
            // open serial port, and use class name for the appName.
            this.serialPort = (SerialPort) portId.open(this.getClass().getName(),
                    TIME_OUT);

            // set port parameters
            this.serialPort.setSerialPortParams(this.dataRate,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            // open the streams
            this.input = new BufferedReader(new InputStreamReader(this.serialPort.getInputStream()));
            this.output = this.serialPort.getOutputStream();

            // add event listeners
            this.serialPort.addEventListener(this);
            this.serialPort.notifyOnDataAvailable(true);

        } catch (final Exception e) {
            System.err.println(e.toString());
        }
    }

    /**
     * This should be called when you stop using the port. This will prevent port locking on platforms like Linux.
     */
    public synchronized void close() {
        if (this.serialPort != null) {
            this.serialPort.removeEventListener();
            this.serialPort.close();
        }
    }

    /**
     * Handle an event on the serial port. Read the data and print it.
     */
    @Override
    public synchronized void serialEvent(final SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                final String inputLine = this.input.readLine();
                this.uis.forEach(t -> t.updateFromRxTx(inputLine));
            } catch (final Exception e) {
                System.err.println(e.toString());
            }
        }
        // Ignore all the other eventTypes, but you should consider the other ones.
    }

    /**
     * Send data.
     *
     * @param data
     *            the String data
     */
    public void sendData(final String data) {
        try {
            this.output.write(data.getBytes(Charset.forName("UTF-8")));
        } catch (final Exception e) {
            Platform.runLater(() -> FXEnvironment.getInstance().showNotificationPopup("Port Error", "Select Port",
                    Duration.MEDIUM, NotificationType.ERROR, null));
        }
    }

    /**
     * Set the port name.
     *
     * @param portName
     *            the {@link String} port name
     */
    public void setPortName(final String portName) {
        this.portName = portName;
    }

    /**
     * Set the bauld rate.
     *
     * @param dataRate
     *            the bauld rate
     */
    public void setDataRate(final int dataRate) {
        this.dataRate = dataRate;
    }

    /**
     * Get the port name.
     *
     * @return the port name
     */
    public String getPortName() {
        return this.portName;
    }

    /**
     * Get the data rate.
     *
     * @return the data rate
     */
    public int getDataRate() {
        return this.dataRate;
    }

}