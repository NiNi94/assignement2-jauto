package com.jauto.model;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import gnu.io.CommPortIdentifier;

/**
 * Show the serial ports available on the PC.
 *
 */
public class SerialPorts {
    private final List<CommPortIdentifier> ports;

    /**
     * Constructor.
     */
    public SerialPorts() {
        this.ports = new ArrayList<>();
    }

    /**
     * Update the list of ports.
     * @throws Exception
     *             the exception
     */
    public void updateListOfPorts() {
        this.ports.clear();
        @SuppressWarnings("unchecked")
        final Enumeration<CommPortIdentifier> portsIo = CommPortIdentifier.getPortIdentifiers();
        while (portsIo.hasMoreElements()) {
            final CommPortIdentifier currPortId = portsIo.nextElement();
            this.ports.add(currPortId);
        }
    }

    /**
     * Get the list of ports.
     * @return the list of ports.
     */
    public List<CommPortIdentifier> getPorts() {
        return this.ports;
    }

}