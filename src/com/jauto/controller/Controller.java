package com.jauto.controller;

import java.util.List;

import com.jauto.view.ui.UIMenu;

import gnu.io.CommPortIdentifier;

/**
 * Interface for {@link UIMenu}, and {@link UIStatistics} Logic.
 *
 */
public interface Controller {
    /**
     * Attache the observer.
     *
     * @param observedUI
     *            the {@link UIMenu}
     */
    void attacheMenuUI(UIMenu observedUI);

    /**
     * Get all the ports avaiable.
     *
     * @return the list of port name
     */
    List<CommPortIdentifier> getPortNames();

    /**
     * Open the port.
     *
     * @param portName
     *            the {@link String} port name
     */
    void openPort(String portName);

    /**
     * Send data throw connection.
     *
     * @param text
     *            the {@link String} text
     */
    void sendData(String text);

    /**
     * Get data from RxTx.
     *
     * @param inputLine
     *            the Data*
     */
    void updateFromRxTx(String inputLine);
}
