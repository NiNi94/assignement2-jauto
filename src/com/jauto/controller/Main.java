package com.jauto.controller;

import java.io.IOException;

import com.jauto.view.FXEnvironment;
import com.jauto.view.screens.MenuScreenController;
import com.jauto.view.voice_recognition.VoiceRecognition;
import com.sun.javafx.application.PlatformImpl;

import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Class main.
 *
 */
@SuppressWarnings("restriction")
public final class Main {

    private Main() {
    }

    /**
     * Main method.
     *
     * @param args
     *            the args
     * @throws IOException
     *             The exception
     */
    public static void main(final String[] args) throws IOException {
        PlatformImpl.startup(() -> {
        });
        final FXEnvironment environment = FXEnvironment.getInstance();

        final Controller controller = ControllerImpl.getInstance();
        final MenuScreenController menuScreenCtrl = new MenuScreenController(environment, controller);
        controller.attacheMenuUI(menuScreenCtrl);

        final VoiceRecognition voiceRecog = VoiceRecognition.getInstance();
        voiceRecog.addEObserver(menuScreenCtrl);

        // NEW TRHEAD FOR JAVAFX
        Platform.runLater(() -> {
            try {
                final Stage primaryStage = new Stage(StageStyle.TRANSPARENT);
                primaryStage.setTitle("JAuto");
                environment.start(primaryStage);
            } catch (final Exception e) {
                System.out.println("Unable to load graphic environment.");
                e.printStackTrace();
            }
            menuScreenCtrl.show();
        });
    }
}
