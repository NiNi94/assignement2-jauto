package com.jauto.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jauto.model.SerialMonitor;
import com.jauto.model.SerialPorts;
import com.jauto.view.ui.UIMenu;

import gnu.io.CommPortIdentifier;
import javafx.application.Platform;

/**
 * Class that manages all the logic of the application.
 */
public final class ControllerImpl implements Controller {

    private static Controller controllerLogicObj;
    private final Set<UIMenu> uis;
    private final SerialPorts ports;
    private final SerialMonitor serialMonitor;

    /**
     * @return The only instance of {@link ControllerImpl}
     */
    public static Controller getInstance() {

        synchronized (ControllerImpl.class) {
            if (controllerLogicObj == null) {

                controllerLogicObj = new ControllerImpl();
            }
        }
        return controllerLogicObj;
    }

    /**
     * The constructor.
     */
    private ControllerImpl() {
        this.uis = new HashSet<>();
        this.ports = new SerialPorts();
        this.serialMonitor = new SerialMonitor(null);
        this.serialMonitor.setObserver(this);
    }

    @Override
    public void attacheMenuUI(final UIMenu observedUI) {
        this.uis.add(observedUI);
    }

    @Override
    public List<CommPortIdentifier> getPortNames() {
        this.ports.updateListOfPorts();
        return this.ports.getPorts();
    }

    @Override
    public void openPort(final String portName) {
        this.serialMonitor.setPortName(portName);
        this.serialMonitor.start();

    }

    @Override
    public void sendData(final String text) {
        this.serialMonitor.sendData(text);
    }

    @Override
    public void updateFromRxTx(final String inputLine) {
        this.uis.forEach(t -> Platform.runLater(() -> t.updateViewReceived(inputLine)));
    }

}